# Enrouleur cable USB

- [French](#français)
- [English](#english)

# Français
## Présentation :
Le projet consiste à réaliser un enrouleur de cable USB, totalement paramètrable, avec le logiciel [FreeCad](https://www.freecadweb.org/).

Je me suis librement inspiré du modèle fait par Manuel Erbse sur thingiverse [USB Cable Reel ](https://www.thingiverse.com/thing:4696231).

## Utilisation :
Vous avez ouvert le fichier, dans la vue combinée de la partie gauche de l'application, cliquez sur l'onglet modèle, vous pouvez alors ouvrir la feuille de calcul pour saisir les dimensions voulues de votre enrouleur. 

![FreeCad 0.19 et USB_Cable_1.0.FCStd](images/freecad_modele.png)

La feuille de calcul est constituée de 3 parties :
1. La zone de saisie : vous pouvez saisir les différentes valeurs pour définir votre enrouleur.
2. La zone des valeurs utilisées : la feuille de calcul vous affiche les valeurs qui seront utilisées pour dessiner l'enrouleur. Ces valeurs tiennent compte de votre saisie précédente, mais aussi de valeurs minimales et maximales (en cas ou) pour obtenir un objet constructible et ne pas casser les différents sketchs des 3 corps.
3. La zone qui affiche une estimation du volume utilisable pour enrouler le cordon et dessous l'estimation de la longueur du cordon pour la configuration définie.

![les différentes zone de la feuille de calcul](images/DifferentesZone.png)

Les différentes valeurs de la zone de saisie correspondent aux dimensions affichées ci-contre. ![les dimensions d'un enrouleur](images/DessinTechnique.png)

Lorque vous êtes satisfait du résultat obtenu, pour exporter en stl les différentes corps de l'enrouleur, il suffit de cliquer sur l'un des coprs dans l'onglet modèle du bandeau de gauche, puis dans le menu "Fichier", de cliquer sur la commande "Exporter ...". Vous devez faire cette opération pour les 3 corps de l'enrouleur.

# English

## Presentation :
The project consists of making a fully configurable USB cable reel with software [FreeCad](https://www.freecadweb.org/).

I was freely inspired by the model made by Manuel Erbse on [USB Cable Reel ](https://www.thingiverse.com/thing:4696231).

## Use :
You have opened the file, in the combined view of the left part of the application, click on the model tab, then you can open the spreadsheet to enter the desired dimensions of your reel.
![FreeCad 0.19 et USB_Cable_1.0.FCStd](images/freecad_modele-english.png)

The spreadsheet consists of 3 parts:

The entry area: you can enter the different values ​​to define your reel.
The used values ​​area: the spreadsheet displays the values ​​that will be used to draw the rewinder. These values ​​take into account your previous entry, but also minimum and maximum values ​​(in case of or) to obtain a constructible object and not to break the various sketches of the 3 bodies.
The area that displays an estimate of the volume that can be used to wind the cord and below the estimate of the length of the cord for the defined configuration.
![les différentes zone de la feuille de calcul](images/DifferentesZone-english.png)


The different values ​​of the entry zone correspond to the dimensions displayed opposite.![les dimensions d'un enrouleur](images/DessinTechnique.png)

When you are satisfied with the result obtained, to export the different reel bodies in stl, all you have to do is click on one of the bodies in the model tab of the left banner, then in the "File" menu, click on the "Export ..." command. You must do this for the 3 reel bodies.
